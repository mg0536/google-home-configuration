""" Test all functionality in Google Home Configuration interface library. """

import google_home_config as ghc
import time
"""  Declare test objects.  """
gh1 = ghc.GoogleHomeConfig("192.168.168.174")
gh2 = ghc.GoogleHomeConfig("192.168.168.175")
gh3 = ghc.GoogleHomeConfig("192.168.168.176")

""" Fetch basic device configuration information """
print("___")
print("eureka info:")
e1 = gh1.get_eureka_info()
e2 = gh2.get_eureka_info()
e3 = gh3.get_eureka_info()
print("Found: '" + e1['name'] + "' at " + e1['ip_address'] + " on SSID: '" + e1['ssid'] + "'")
print("Found: '" + e2['name'] + "' at " + e2['ip_address'] + " on SSID: '" + e2['ssid'] + "'")
print("Found: '" + e3['name'] + "' at " + e3['ip_address'] + " on SSID: '" + e3['ssid'] + "'")
print

""" Fetch wifi network information """
print("___")
print("wifi configured networks:")
cn1 = gh1.get_wifi_configured_networks()
cn2 = gh2.get_wifi_configured_networks()
cn3 = gh3.get_wifi_configured_networks()
print("  + '" + e1['name'] + "' -> " + str(len(cn1)) + " networks configured:")
for net in cn1:
    print("    + '" + net['ssid'] + "'")
print("  + '" + e2['name'] + "' -> " + str(len(cn2)) + " networks configured:")
for net in cn2:
    print("    + '" + net['ssid'] + "'")
print("  + '" + e3['name'] + "' -> " + str(len(cn3)) + " networks configured:")
for net in cn3:
    print("    + '" + net['ssid'] + "'")
    
""" Command GH to do a wifi scan. """
print("___")
print("wifi scan:")
print("Scanning from " + e1['name'] + ",")
gh1.do_wifi_scan()
print("Scanning from " + e2['name'] + ",")
gh2.do_wifi_scan()
print("Scanning from " + e3['name'] + ",")
gh3.do_wifi_scan()
print("Allowing scans to complete...")
time.sleep(0.3)
wsr1 = gh1.get_wifi_scan_results()
print("==> '" + e1['name'] + "' found: ")
for dev in wsr1:
    print("  + SSID: '" + dev['ssid'] + "'  signal_level=" + str(dev['signal_level']))
wsr2 = gh2.get_wifi_scan_results()
print("==> '" + e2['name'] + "' found: ")
for dev in wsr2:
    print("  + SSID: '" + dev['ssid'] + "'  signal_level=" + str(dev['signal_level']))
wsr3 = gh3.get_wifi_scan_results()
print("==> '" + e3['name'] + "' found: ")
for dev in wsr3:
    print("  + SSID: '" + dev['ssid'] + "'  signal_level=" + str(dev['signal_level']))


""" Fetch bluetooth status information """
print("___")
print("bluetooth info:")
bt1 = gh1.get_bluetooth_status()
bt2 = gh2.get_bluetooth_status()
bt3 = gh3.get_bluetooth_status()
print("  + '" + e1['name'] +"': audio_mode="+str(bt1['audio_mode']) + "  connected_devices=" + str(len(bt1['connected_devices'])))
print("  + '" + e2['name'] +"': audio_mode="+str(bt2['audio_mode']) + "  connected_devices=" + str(len(bt2['connected_devices'])))
print("  + '" + e3['name'] +"': audio_mode="+str(bt3['audio_mode']) + "  connected_devices=" + str(len(bt3['connected_devices'])))


""" Command GH to do a bluetooth scan. """
print("___")
print("bluetooth scan:")
print("Scanning from " + e1['name'] + ",")
gh1.do_bt_scan(10.0, False) # scan duration in seconds
print("scanning from " + e2['name'] + ",")
gh2.do_bt_scan(10.0, False)
print("scanning from " + e3['name'] + ",")
gh3.do_bt_scan(10.0, False)
print("Allowing scans to complete...")
time.sleep(10.0)
bsr1 = gh1.get_bt_scan_results()
print("==> '" + e1['name'] + "' found: ")
for dev in bsr1:
    print("  + Device:[" + dev['mac_address'] + "]  RSSI=" + str(dev['rssi']) + "  Name: '" + dev['name'] + "'")
bsr2 = gh2.get_bt_scan_results()
print("==> '" + e2['name'] + "' found: ")
for dev in bsr2:
    print("  + Device:[" + dev['mac_address'] + "]  RSSI=" + str(dev['rssi']) + "  Name: '" + dev['name'] + "'")
bsr3 = gh3.get_bt_scan_results()
print("==> '" + e3['name'] + "' found: ")
for dev in bsr3:
    print("  + Device:[" + dev['mac_address'] + "]  RSSI=" + str(dev['rssi']) + "  Name: '" + dev['name'] + "'")

""" Fetch alarm and timer information """
print("___")
print("alarm and timer info:")
at1 = gh1.get_alarms()
print("==> '" + e1['name'] + "' has " + str(len(at1['alarm'])) + " alarms: ")
for alarm in at1['alarm']:
    print("  + ID: '" + alarm['id'] + "' status: " + str(alarm['status']) + " time: " + 
          str(alarm['time_pattern']['hour']) + ":" + str(alarm['time_pattern']['minute']))
print("==> '" + e1['name'] + "' has " + str(len(at1['timer'])) + " timers: ")
for timer in at1['timer']:
    print("  + ID: '" + timer['id'] + "' status:" + str(timer['status']) + str(timer['fire_time']))

at2 = gh2.get_alarms()
print("==> '" + e2['name'] + "' has " + str(len(at2['alarm'])) + " alarms: ")
for alarm in at2['alarm']:
    print("  + ID: '" + alarm['id'] + "' status: " + str(alarm['status']) + " time: " + 
          str(alarm['time_pattern']['hour']) + ":" + str(alarm['time_pattern']['minute']))
print("==> '" + e2['name'] + "' has " + str(len(at2['timer'])) + " timers: ")
for timer in at2['timer']:
    print("  + ID: '" + timer['id'] + "' status:" + str(timer['status']) + str(timer['fire_time']))
at3 = gh3.get_alarms()
print("==> '" + e3['name'] + "' has " + str(len(at3['alarm'])) + " alarms: ")
for alarm in at3['alarm']:
    print("  + ID: '" + alarm['id'] + "' status: " + str(alarm['status']) + " time: " + 
          str(alarm['time_pattern']['hour']) + ":" + str(alarm['time_pattern']['minute']))
print("==> '" + e3['name'] + "' has " + str(len(at3['timer'])) + " timers: ")
for timer in at3['timer']:
    print("  + ID: '" + timer['id'] + "' status:" + str(timer['status']) + str(timer['fire_time']))

tv1=gh1.get_alarm_volume()
print("==> '" + e1['name'] + "' has alarm volume=" + str(tv1['volume']))
tv2=gh2.get_alarm_volume()
print("==> '" + e2['name'] + "' has alarm volume=" + str(tv2['volume']))
tv3=gh3.get_alarm_volume()
print("==> '" + e3['name'] + "' has alarm volume=" + str(tv3['volume']))
print
print("==> Setting '" + e1['name'] + "' alarm volume= 0.8")
gh1.set_alarm_volume(0.8)
tv1=gh1.get_alarm_volume()
print("==> '" + e1['name'] + "' has alarm volume=" + str(tv1['volume']))


