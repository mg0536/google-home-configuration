"""Get a long-term data set of BT at home, testing the GHC library. """

"""Configure the logger for this data set. """
import logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)
# create file handler which logs even debug messages
fh = logging.FileHandler('btscan.log')
fh.setLevel(logging.INFO)
# create console handler with a lower log level (see it all, log just the data at INFO level)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
fh.setFormatter(formatter)
ch.setFormatter(formatter)
# add the handlers to the logger
logger.addHandler(fh)
logger.addHandler(ch)


import google_home_config as ghc
import time
"""  Declare test objects.  """
gh1 = ghc.GoogleHomeConfig("192.168.168.174")
gh2 = ghc.GoogleHomeConfig("192.168.168.175")
gh3 = ghc.GoogleHomeConfig("192.168.168.176")


""" Fetch basic device configuration information """
print("___")
print("eureka info:")
e1 = gh1.get_eureka_info()
e2 = gh2.get_eureka_info()
e3 = gh3.get_eureka_info()
logger.debug("Found: '" + e1['name'] + "' at " + e1['ip_address'] + " on SSID: '" + e1['ssid'] + "'")
logger.debug("Found: '" + e2['name'] + "' at " + e2['ip_address'] + " on SSID: '" + e2['ssid'] + "'")
logger.debug("Found: '" + e3['name'] + "' at " + e3['ip_address'] + " on SSID: '" + e3['ssid'] + "'")

while True:
    for x in range(2): # do 2 scans in a minute, then wait 4 minutes
        """ Command GH to do a bluetooth scan. """
        logger.debug(" Scanning")
        gh1.do_bt_scan(7.0, True) # scan duration in seconds, True=> clear previous results
        gh2.do_bt_scan(7.0, True)
        gh3.do_bt_scan(7.0, True)
        time.sleep(8.0)
        logger.debug("Scan complete")
        bsr1 = gh1.get_bt_scan_results()
        for dev in bsr1:
            logger.info("Scanner='" + e1['name'] + "' Found='" + dev['mac_address'] + "' RSSI=" + str(dev['rssi']) + "  Name: '" + dev['name'] + "'")
        bsr2 = gh2.get_bt_scan_results()
        for dev in bsr2:
            logger.info("Scanner='" + e2['name'] + "' Found='" + dev['mac_address'] + "' RSSI=" + str(dev['rssi']) + "  Name='" + dev['name'] + "'")
        bsr3 = gh3.get_bt_scan_results()
        for dev in bsr3:
            logger.info("Scanner='" + e3['name'] + "' Found='" + dev['mac_address'] + "' RSSI=" + str(dev['rssi']) + " Name='" + dev['name'] + "'")
        logger.debug("waiting 22 seconds")    
        time.sleep(22.0)
    logger.debug("waiting 1 minute")    
    time.sleep(60.0)
