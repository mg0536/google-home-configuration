==========================================
 Google Home Configuration Library
==========================================
|
^^^^^^^^^^
Overview
^^^^^^^^^^
|
The Google Home is an embedded device providing a speech interface to many Google cloud services. 
These devices also present an interface to the Google Home mobile app, for configuration.

This package implements a Python interface to a Google Home device, using this configuration API.

|
^^^^^^^^^^
Changelog
^^^^^^^^^^
- 2018-07-08  - Version 0.38:  Added google_discover() simple discovery on subnet
- 2018-07-08  - Version 0.37:  Added timeout to every service call ( 100 msec - very conservative).
- 2018-07-08  - Version 0.36:  Added do_reboot(), and get_uptime().
- 2018-07-08  - Version 0.35:  Added basic alarm & timer support.
- 2018-07-08  - Version 0.30:  BT scanning works. WiFi scanning works. Device configuration reading works.

|
^^^^^^^^^^^^^^^
 API Reference
^^^^^^^^^^^^^^^

This library was developed using the `Google Home Local API <https://rithvikvibhu.github.io/GHLocalApi/>`_

Thanks to Rithvik Vibhu for making this available.


