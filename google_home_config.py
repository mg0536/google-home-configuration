""" Google Home Configuration interface Library
    Copyright 2018, Matthew Grund.
    Based on the 'Google Home Local API' published by Rithvik Vibhu: 
       https://rithvikvibhu.github.io/GHLocalApi/#device-info-assistant-check-ready-status-post
"""

import requests
import json
import time
import sys
    
class GoogleHomeConfig(object):
    """ Establish some class constants. """
    JSON_HEADERS = {'Content-type': 'application/json', 'Accept': 'text/plain'}
    
    def __init__(self,address):
        """ Initialize instance data. """
        self.address = address
        self.port = 8008
        self.is_up = False
        self.has_eureka_info = False
        self.has_bt_status = False
        self.has_bt_scan_data = False
        self.has_wifi_scan_data = False
        self.has_alarm_timer_data = False
        self.last_wifi_scan_time = 0
        self.last_bt_scan_time = 0
        self.last_bt_scan_duration = 0
        self.last_eureka_time = 0
        
    def version(self):    
        return("0.39")
    
    """ 
          Basic Google Home system information
    """      
    def get_eureka_info(self):
        """ Fetch device eureka info. """
        get_url =  ( "http://" + self.address + ":" + str(self.port) + 
            "/setup/eureka_info?options=detail&param=version,build_info,name,detail,device_info" )
        req = requests.get(get_url, headers=self.JSON_HEADERS, timeout=3.0)
        results = json.loads(req.text)
        self.last_eureka_info = results
        self.last_eureka_time = time.time()
        return(results)
    
    def do_reboot(self):
        """ Command the Google Home to reboot. Takes a few seconds to respond to the command, then many more to reboot."""
        reset_url = "http://" + self.address + ":" + str(self.port) + "/setup/reboot"
        data = {'params': 'now'}
        req = requests.post(reset_url, data=json.dumps(data), headers=self.JSON_HEADERS, timeout=0.500)
        
        """ Check return code. """
        if req.status_code != 200:
            print("error: request returned status code " + req.status_code)
            return
        self.is_up = False
        return req.text
    
    def get_address(self):
        """ Fetch eureka info and parse for ip address. """
        e = self.get_eureka_info()        
        return(e['ip_address'])
    
    def get_uptime(self):
        """ Fetch eureka info and parse for uptime. """
        e = self.get_eureka_info()        
        return(e['uptime'])
    
    def get_name(self):
        """ Fetch eureka info and parse for name. """
        e = self.get_eureka_info()        
        return(e['name'])    

    def get_ssid(self):
        """ Fetch eureka info and parse for ssid currently connected. """
        e = self.get_eureka_info()        
        return(e['ssid'])   
     
    def get_offer(self):
        """ Fetch offer token for use with cloud api. """
        get_url = "http://" + self.address + ":" + str(self.port) + "/setup/offer"
        try:
            req = requests.get(get_url, headers=self.JSON_HEADERS, timeout=0.050)
        except:
            return    
        results = json.loads(req.text)
        token = results['token']
        return(token)        
    
    """
           BLuetooth Subsystem below.
    """        
    def get_bluetooth_status(self):
        """ Fetch device bluetooth info. """
        get_url = "http://" + self.address + ":" + str(self.port) + "/setup/bluetooth/status"
        req = requests.get(get_url, headers=self.JSON_HEADERS)
        results = json.loads(req.text)
        return(results)
        
    def do_bt_scan(self,scan_duration_sec, clear_previous_results=True):
        """ Command the Google Home to start a Bluetooth scan. """
        scan_url = "http://" + self.address + ":" + str(self.port) + "/setup/bluetooth/scan"
        data = {'enable': True, 'timeout': scan_duration_sec, 'clear_results': clear_previous_results}
        req = requests.post(scan_url, data=json.dumps(data), headers=self.JSON_HEADERS, timeout=0.100 )
        
        """ Check return code. """
        if req.status_code != 200:
            print("error: request returned status code " + req.status_code)
            return
        self.is_up = True
        self.last_bt_scan_time = time.time()
        self.last_bt_scan_duration = scan_duration_sec 
        return req.text
    
    def get_bt_scan_results(self):
        """ Fetch results of last bluetooth scan. """
        get_url = "http://" + self.address + ":" + str(self.port) + "/setup/bluetooth/scan_results"
        req = requests.get(get_url, headers=self.JSON_HEADERS, timeout=0.100)
        results = json.loads(req.text)
        return(results)
    
    
    """
           WIFI Subsystem below.
    """    
    def do_wifi_scan(self):
        """ Command the Google Home to start a WiFi scan. (note - this seems to happen automatically, perhaps not needed.)"""
        scan_url = "http://" + self.address + ":" + str(self.port) + "/setup/scan_wifi"
        req = requests.post(scan_url, headers=self.JSON_HEADERS, timeout=0.100 )
        
        """ Check return code. """
        if req.status_code != 200:
            print("error: request returned status code " + req.status_code)
            return
        """ Got a good reply. """
        self.is_up = True
        self.last_wifi_scan_time = time.time()
        return req.text

    def get_wifi_configured_networks(self):
        """ Fetch network configurations. """
        get_url = "http://" + self.address + ":" + str(self.port) + "/setup/configured_networks"
        req = requests.get(get_url, headers=self.JSON_HEADERS, timeout=0.100)
        results = json.loads(req.text)
        return(results)

    def get_wifi_scan_results(self):
        """ Fetch wifi scan results. """
        get_url = "http://" + self.address + ":" + str(self.port) + "/setup/scan_results"
        req = requests.get(get_url, headers=self.JSON_HEADERS, timeout=0.100)
        results = json.loads(req.text)
        return(results)
    
    """
           Alarm & Timer Subsystem below.
    """    
    def get_alarms(self):
        """ Fetch alarms and timers set on the device. """
        get_url = "http://" + self.address + ":" + str(self.port) + "/setup/assistant/alarms"
        req = requests.get(get_url, headers=self.JSON_HEADERS, timeout=0.100)
        results = json.loads(req.text)
        return(results)
    
    def get_alarm_volume(self):
        """ Fetch alarm volume set on the device. """
        post_url = "http://" + self.address + ":" + str(self.port) + "/setup/assistant/alarms/volume"
        req = requests.post(post_url, headers=self.JSON_HEADERS, timeout=0.100)
        results = json.loads(req.text)
        return(results)        
    
    def set_alarm_volume(self,volume_level):
        """ Configure alarm volume. """
        post_url = "http://" + self.address + ":" + str(self.port) + "/setup/assistant/alarms/volume"
        data = {'volume': volume_level}
        req = requests.post(post_url, data=json.dumps(data),headers=self.JSON_HEADERS, timeout=0.100)
        results = json.loads(req.text)
        return(results) 

"""methods outside of class context, to be used for discovery"""

def get_offer_by_address(address):
    """ Fetch offer token for use with cloud api. """
    get_url = "http://" + address + ":8008" + "/setup/offer"
    JSON_HEADERS = {'Content-type': 'application/json', 'Accept': 'text/plain'}
    try:
        req = requests.get(get_url, headers=JSON_HEADERS, timeout=0.3)
    except:
        return "ERROR"
            
    results = json.loads(req.text)
    token = results['token']
    return(token)    

def google_discover(subnet):
    """ Discover all Google devices on subnet."""
    ghosts = []
    byte1,byte2,byte3,byte4 = subnet.split('.')
    prefix = byte1 + "." + byte2 + "." + byte3 + "."
    for byte in range(0,254):
        address = prefix + str(byte)
        l=len(get_offer_by_address(address))
        result = "Nope\r"
        if l > 5 :
            ghosts.append(address)
            result = "Yep   \r\n"
            
        sys.stdout.write(address + ": " + result)
    print(str(len(ghosts)) + " Google devices found.")  
    print              
    for dev in ghosts:
        g = GoogleHomeConfig(dev)
        e = g.get_eureka_info()
        print("Found [" + e['ip_address'] +"]: '"+ e['name'] + "' a '" + e['detail']['model_name'] + "' rev: " + 
              e['cast_build_revision'] )
        
    return ghosts